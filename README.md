Project configuration for LifeRay patch (extending/overriding LifeRay classes, implementation of global and project classes)

Modules:

1. ant-liferay-patch-sdk

    Module for LifeRay patch with Ant

2. maven-liferay-patch-sdk

    Module for LifeRay patch with Maven2
